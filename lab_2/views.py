from django.shortcuts import render
from .models import Note
from django.http.response import HttpResponse
from django.core import serializers

# Create your views here.
def index_HTML(request):
    note = Note.objects.all().values()  # TODO Implement this
    response = {'note': note}
    return render(request, 'note_list_lab2.html', response)

def index_XML(data):
    data = serializers.serialize('xml', Note.objects.all())  # TODO Implement this
    return HttpResponse(data, content_type="application/xml")

def index_JSON(data):
    data = serializers.serialize('json', Note.objects.all())  # TODO Implement this
    return HttpResponse(data, content_type="application/json")