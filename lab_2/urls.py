from django.urls import path
from .views import index_XML
from .views import index_HTML
from .views import index_JSON

urlpatterns = [
    path('', index_HTML, name='html'),
    path('xml', index_XML, name='xlm'),
    path('json', index_JSON, name='json')
    ]
    # TODO Add friends path using friend_list Views