from django import forms
from lab_3.models import Friend

class FriendForm(forms.ModelForm):
    class Meta:
        model = Friend
        fields = ['name',
                  'npm',
                  'date_of_birth'
                  ]
