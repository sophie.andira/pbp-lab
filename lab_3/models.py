from django.db import models
from datetime import datetime


class Friend(models.Model):
    name = models.CharField(max_length=30)
    npm = models.CharField(max_length=10)
    date_of_birth = models.DateField(default=datetime.now)

