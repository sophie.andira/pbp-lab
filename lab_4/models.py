from django.db import models

class Note(models.Model):
    to = models.CharField(max_length=30)
    from_who = models.CharField(max_length=30)
    title = models.CharField(max_length=30)
    message = models.CharField(max_length=30)

