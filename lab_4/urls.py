from .views import index, note_list, add_note
from django.urls import path

urlpatterns = [
    path('', index, name='index'),
    path('add-note/', add_note, name='add_note'),
    path('note-list/', note_list, name='note_list'),
]
