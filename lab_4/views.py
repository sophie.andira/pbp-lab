from django.shortcuts import render, redirect
from .forms import NoteForm
from .models import Note

def index(request):
    note = Note.objects.all().values()  # TODO Implement this
    response = {'note': note}
    return render(request, 'lab4_index.html', response)

def add_note(request):
    context = {}

    form = NoteForm(request.POST or None)
    if (form.is_valid() and request.method == 'POST'):
        form.save()
        return redirect('/lab-4')

    context['form'] = form
    return render(request, 'lab4_form.html', context)

def note_list(request):
    note = Note.objects.all()  # TODO Implement this
    response = {'note': note}
    return render(request, 'lab4_note_list.html', response)

