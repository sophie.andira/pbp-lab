import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

void main() {
  runApp(MyApp());
}

@immutable
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    const title = 'Dapatkan barang yang Anda perlukan di sini';
    final arr = [
      'Thermometer.jpg',
      'Masker.jpg',
      'Obat-obatan.jpg',
      'Strap-Mask.jpg',
      'Hand-Sanitizer.jpg',
      'Vitamin.png',
      'Face-Shield.jpg',
      'Oximetry.jpeg'
    ];

    return MaterialApp(
      title: title,
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: const Color(0xffad97ff),
          title: const Text(title),
        ),
        body: GridView.count(
          crossAxisCount: 2,
          children: List.generate(arr.length, (index) {
            return Center(
              child: Container(
                  margin: const EdgeInsets.all(10.0),
                  width: 250.0,
                  height: 266.0,
                  child: Container(
                      child: Column(children: <Widget>[
                    Image.asset(arr[index]),
                    Text(arr[index].split(".")[0])
                  ]))),
            );
          }),
        ),
      ),
    );
  }
}
