import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

//LAB7
void main() {
  runApp(MyApp());
}

@immutable
class MyApp extends StatelessWidget {
   const MyApp({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    const title = 'Dapatkan barang yang Anda perlukan di sini';
    final arr = [
      'Thermometer.jpg',
      'Masker.jpg',
      'Obat-obatan.jpg',
      'Strap-Mask.jpg',
      'Hand-Sanitizer.jpg',
      'Vitamin.png',
      'Face-Shield.jpg',
      'Oximetry.jpeg'
    ];

    return MaterialApp(
      title: title,
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: const Color(0xffad97ff),
          title: const Text(title),
        ),
        body: 
        // const MyCustomForm(), ini ditaro dimana ya gw bingung
        GridView.count(
          crossAxisCount: 2,
          children: List.generate(arr.length, (index) {
            return Center(
              child: Container(
                  margin: const EdgeInsets.all(10.0),
                  width: 250.0,
                  height: 400.0,
                  child: Container(
                      child: Column(children: <Widget>[
                    Image.asset(arr[index]),
                    Text(arr[index].split(".")[0]),
                    const MyCustomForm(),
                  ]))),
            );
          }),
        ),
      ),
    );
  }
}

class MyCustomForm extends StatefulWidget {
  const MyCustomForm({Key? key}) : super(key: key);

  @override
  MyCustomFormState createState() {
    return MyCustomFormState();
  }
}

class MyCustomFormState extends State<MyCustomForm> {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          TextFormField(
            validator: (value) {
              if (value == null || value.isEmpty) {
                return 'Apakah Anda puas terhadap pencarian Anda?';
              }
              return null;
            },
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 16.0),
            child: ElevatedButton(
              onPressed: () {
                if (_formKey.currentState!.validate()) {
                  ScaffoldMessenger.of(context).showSnackBar(
                    const SnackBar(content: Text('Terima Kasih')),
                  );
                }
              },
              child: const Text('Submit'),
            ),
          ),
        ],
      ),
    );
  }
}

